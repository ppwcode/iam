resource "aws_iam_group" "devsecops" {
  name = "devsecops"
  path = "/human/"
}

resource "aws_iam_group_policy_attachment" "devsecops-read_almost_anything" {
  group      = "${aws_iam_group.humans.name}"
  policy_arn = "${aws_iam_policy.read_almost_anything.arn}"
}

resource "aws_iam_group_policy_attachment" "devsecops-iam-read" {
  group      = "${aws_iam_group.devsecops.name}"
  policy_arn = "${aws_iam_policy.iam-read.arn}"
}

resource "aws_iam_group_policy_attachment" "devsecops-view_billing" {
  group      = "${aws_iam_group.devsecops.name}"
  policy_arn = "${aws_iam_policy.billing-view.arn}"
}

resource "aws_iam_group_policy_attachment" "devsecops-manage_users-devsecops" {
  group      = "${aws_iam_group.devsecops.name}"
  policy_arn = "${aws_iam_policy.iam-manage_users-devsecops.arn}"
}

resource "aws_iam_group_policy_attachment" "devsecops-tfstate-readwrite" {
  group      = "${aws_iam_group.devsecops.name}"
  policy_arn = "${aws_iam_policy.tfstate-readwrite_nodelete_nor_change.arn}"
}

resource "aws_iam_group_policy_attachment" "devsecops-manage-roles" {
  group      = "${aws_iam_group.devsecops.name}"
  policy_arn = "${aws_iam_policy.iam-manage_roles-devsecops.arn}"
}

resource "aws_iam_group_policy_attachment" "devsecops-test" {
  group      = "${aws_iam_group.devsecops.name}"
  policy_arn = "${aws_iam_policy.iam-assume_role-test.arn}"
}
