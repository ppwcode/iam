// https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_delegate-permissions_examples.html#iampolicy-example-userlistall
// https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_delegate-permissions_examples.html#creds-policies-users

data "aws_iam_policy_document" "iam-read" {
  version = "2012-10-17"

  statement {
    sid    = "AllowReadAndReportIam"
    effect = "Allow"

    actions = [
      "iam:Get*",
      "iam:List*",
      "iam:Generate*",
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_policy" "iam-read" {
  name = "IamReadAndReport"
  path = "/iam/"

  description = "users can see all other users, groups and policies, and create credentials reports (so they can find out what they are missing)"

  policy = "${data.aws_iam_policy_document.iam-read.json}"
}
