terraform {
  backend "s3" {
    bucket         = "tfstate.ppwcode.org"
    key            = "iam.tfstate"
    region         = "eu-west-1"
    profile        = "ppwcode"
    encrypt        = true
    dynamodb_table = "tfstate-lock.ppwcode.org"
  }
}
