// https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_delegate-permissions_examples.html#creds-policies-credentials

data "aws_iam_policy_document" "iam-self_manage_credentials" {
  version = "2012-10-17"

  statement {
    sid    = "AllowSelfManageCreds"
    effect = "Allow"

    actions = [
      "iam:*LoginProfile",
      "iam:*ChangePassword",
      "iam:*AccessKey*",
      "iam:*SSHPublicKey*",
    ]

    resources = [
      "arn:aws:iam::*:user/human/*$${aws:username}",
    ]
  }

  statement {
    sid    = "AllowListUsersAndAccountSettingsForConsole"
    effect = "Allow"

    actions = [
      "iam:ListAccount*",
      "iam:GetAccountSummary",
      "iam:GetAccountPasswordPolicy",
      "iam:ListUsers",
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_policy" "iam-self_manage_credentials" {
  name = "IamSelfManageCredentials"
  path = "/iam/"

  description = "users can manage their passwords, access and ssh keys themselves"

  /* *LoginProfile* includes changePassword, according to the doc */

  policy = "${data.aws_iam_policy_document.iam-self_manage_credentials.json}"
}
