// NOTE: There is only a single account alias per AWS account.

resource "aws_iam_account_alias" "organization" {
  account_alias = "ppwcode"
}
