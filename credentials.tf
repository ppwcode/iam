locals {
  region  = "eu-west-1"
  profile = "ppwcode"
}

provider "aws" {
  region  = "${local.region}"
  profile = "${local.profile}"
  version = "~> 2.8"
}
