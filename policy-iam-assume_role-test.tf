data "aws_iam_policy_document" "iam-assume_role-test" {
  version = "2012-10-17"

  statement {
    sid    = "AllowIamAssumeRoleTest"
    effect = "Allow"

    actions = [
      # allow users to assume these roles
      "sts:AssumeRole",
    ]

    resources = [
      "arn:aws:iam::${data.aws_caller_identity.caller.account_id}:role/test/*",
    ]
  }
}

resource "aws_iam_policy" "iam-assume_role-test" {
  name = "IamAssumeRoleTest"
  path = "/iam/"

  description = "can read assume roles that have the /role/test/* path"

  policy = "${data.aws_iam_policy_document.iam-assume_role-test.json}"
}
