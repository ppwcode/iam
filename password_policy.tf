# NOTE: There is only a single policy allowed per AWS account. An existing policy will be lost when using this
#       resource as an effect of this limitation. */

# All users should use MFA. This policy is intended not to be obnoxious, but be "unusable" without a password manager
# (minimum 12 characters). A change is required every year.

resource "aws_iam_account_password_policy" "policy" {
  minimum_password_length        = 12
  require_uppercase_characters   = false
  require_lowercase_characters   = true
  require_numbers                = true
  require_symbols                = false
  allow_users_to_change_password = true
  max_password_age               = 365
  password_reuse_prevention      = 3
  hard_expiry                    = false
}
