# Devsecops can define roles, and manage a subset of users. Be careful.
#
# This policy does not describe Get*, List*, or Generate* actions, because we assume policy users already got those
# rights from `policy-iam-read.tf` and `policy-read_anything.tf`.

# https://docs.aws.amazon.com/IAM/latest/UserGuide/list_iam.html

data "aws_iam_policy_document" "iam-manage_users-devsecops" {
  version = "2012-10-17"

  statement {
    sid    = "AllowIamHumanAndDevsecopsGroupManagement"
    effect = "Allow"

    actions = [
      "iam:AddUserToGroup",
      "iam:RemoveUserFromGroup",
      "iam:PutGroupPolicy",
      "iam:AttachGroupPolicy",
      "iam:DetachGroupPolicy",
      "iam:DeleteGroupPolicy",
    ]

    resources = [
      "${aws_iam_group.humans.arn}",
      "${aws_iam_group.devsecops.arn}",
    ]
  }

  statement {
    sid    = "AllowIamCiAndHumanUserManagement"
    effect = "Allow"

    actions = [
      "iam:CreateUser",
      "iam:UpdateUser",
      "iam:DeleteUser",
      "iam:TagUser",
      "iam:UntagUser",
      "iam:CreateLoginProfile",
      "iam:UpdateLoginProfile",
      "iam:DeleteLoginProfile",
    ]

    resources = [
      "arn:aws:iam::${data.aws_caller_identity.caller.account_id}:user/ci/*",
      "arn:aws:iam::${data.aws_caller_identity.caller.account_id}:user/human/*",
    ]
  }

  statement {
    sid    = "AllowIamSimulation"
    effect = "Allow"

    actions = [
      "iam:Simulate*",
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_policy" "iam-manage_users-devsecops" {
  name = "IamManageUsersDevsecops"
  path = "/iam/"

  description = "can manage humand and ci users, human and devsecops group"

  policy = "${data.aws_iam_policy_document.iam-manage_users-devsecops.json}"
}
