data "aws_iam_policy_document" "billing-view" {
  version = "2012-10-17"

  statement {
    sid    = "AllowViewBillingAndUsage"
    effect = "Allow"

    actions = [
      "aws-portal:ViewBilling",
      "aws-portal:ViewUsage",
      "budgets:ViewBudget",
      "budgets:ModifyBudget",
      "cur:*",
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_policy" "billing-view" {
  name = "BillingViewAndUsage"
  path = "/billing/"

  description = "can view billing, view and manage budgets, and reports"

  policy = "${data.aws_iam_policy_document.billing-view.json}"
}
