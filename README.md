# AWS IAM

Declarative definition of the organisation's AWS IAM users, roles and policies.

The main administrative users, who have full power, are not defined in this configuration, to avoid lockout in case of a
mistake. These users bootstrap the IAM identities.

## Structure

This [Terraform] configuration uses the [ppwcode conventions][terraform].

- Human users are defined in [`users_human.tf`](users_human.tf). Note that _super users_ are not defined in this
  configuration, to prevent lock-out.
- `policy-SUBJECT-….tf` defines a policy; policies should be attached to groups, specific machine and `/ci/` users and
  roles, and to specific `/human/` users in exceptional cases.
- `group-….tf` defines a group, its policies, and members
  - [`group-humans.tf`](group-humans.tf) defines `/general/humans`, and makes it possible for human users to manage
    themselves
  - [`group-devsecops.tf`](group-devsecops.tf) defines `/human/devsecops`, for human devsecops colleagues. See below.
- [`password_policy.tf`](password_policy.tf) defines the unique organization's account password policy for human users.
  All human users must enable MFA for console access (_this is not yet enforced with a policy_).
- [`account_alias.tf`](account_alias.tf) defines the unique organisation's account alias (_ppwcode_)

To keep a constant way of configuring users, the module [`user/`](user/README.md) is used for every human user.

## Getting started

The infrastructure is defined using [Terraform].

See [Getting started with a Terraform configuration].

## Strategy

### Human users

Human users have the ability to self-manage their account and credentials via the console. All human users are required
to enable MFA on their account.

All human IAM users must be member of the group [`humans`](group-humans.tf).

### Execution roles for applications deployed on AWS

Operational machines and processes often need access to other resources to do their job. When possible, an execution
role is created for the operational machines and processes, to which the necessary policies are attached. By exception,
an IAM user can be used instead. Several roles or users can be defined for different environments or builds.

Applications deployed on AWS (Lambda functions, API Gateway, programs on ECS-deployed Docker images or EC2 instances, …)
gain access to other AWS assets using roles. The necessary roles are defined in the same configuration as the
infrastructure, in a subdirectory (`terraform/infrastructure`) of the git repository where the code of the application
is maintained.

The configuration defines one or more execution policies (~ `terraform/infrastructure/policy-execution.tf`) that allows
all the necessary actions on exact resources, possibly parametrized for different environments. An execution role (~
`terraform/infrastructure/role-execution.tf`) is defined, to which this policy is attached. Similar roles can be defined
for different environments if desired. The AWS application execution assets are defined to use this role.

The name of the role _SHOULD_ be of the form `role/execution/<GIT REPO NAME>/<ENVIRONMENT>`.

If differentiated, the `<ENVIRONMENT>` for production is `production`. The `<ENVIRONMENT>` for staging environments is
`staging-DDDDD`, where `DDDDD` is a number zero-padded to 5 digits. The `<ENVIRONMENT>` for development environments is
`dev-USER`, where `USER` is the name of the developer, or another identifier for team efforts (like the date).

Execution roles do not allow changes to the application infrastructure.

### Execution roles for CI

The Continuous Integration system executes unit tests and integration tests for parts of the code of the application,
and thus in general requires the same abilities as the execution role, possibly in a separate environment.

Apart from that, unit and integration tests often need the ability to set up a context for a test in the infrastructure,
and to clean up test residue after the test. Therefor, a second policy (~ `terraform/infrastructure/policy-test.tf`) is
defined that allows these actions on exact resources.

Furthermore, CI often needs to be able to deploy artifacts.

_**// MUDO:** separate role? or only policy, and include all in devsecops role? than tests assume ≠!_

Tests are created to perform operations on AWS infrastructure after assuming a test role. Each application configuration
defines a test role (~ `terraform/infrastructure/role-test.tf`), to which both the execution and test policy are
attached. The name of the role _MUST_ be of the form `role/test/<GIT REPO NAME>`.

All IAM users that run the tests must be able to assume this role. Therefor, the role has a trust policy that includes

    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Principal": {
            "AWS": "*"
          },
          "Action": "sts:AssumeRole"
        }
      ],
      "Condition": {
        "StringLike": {
          "aws:PrincipalTag/canAssumeRole": "*test*"
        }
      }
    }

This configuration defines one or more CI users, gathered in the group `ci`. The intention is to define 1 (or more) CI
user per CI system. It's credentials should be stored in CI environment variables `AWS_ACCESS_KEY_ID` and
`AWS_SECRET_ACCESS_KEY` (making sure the latter is treated as sensitive data). That way, the AWS SDK can authenticate by
convention, and it is not necessary to defined a CI user per application.

CI users can assume test roles when they are tagged (see below) with

    "canAssumeRole": "test"

Apart from that, their group policy allows them to assume the test roles, because it includes

    {
      "Version": "2012-10-17",
      "Statement": {
        "Effect": "Allow",
        "Action": "sts:AssumeRole",
        "Resource": "arn:aws:iam::ACCOUNT-ID-WITHOUT-HYPHENS:role/test/*"
      }
    }

[AWS Documentation » AWS Security Token Service » API Reference » Actions » AssumeRole](https://docs.aws.amazon.com/STS/latest/APIReference/API_AssumeRole.html)
documents the assume role API call.

Test roles do not allow changes to the application infrastructure.

#### Devsecops

`devsecops` users work as a team, and are responsible for all AWS resources of all applications as a team. This includes
responsibility for the cost of using the AWS resources.

An IAM user is a `devsecops` user when she is a member of the [`devsecops`](group-devsecops.tf) group. All
[`devsecops`](group-devsecops.tf) members must be [`humans`](group-humans.tf).

Access to resources is limited for `devsecops` users as a defense _to avoid mistakes_ as much as possible, not because
we want tight control. Devsecops users do not get access to AWS account payment parameters, but can see the amount of
money spend. This is necessary to be able to take responsibility over the cost of using AWS resources. Apart from that,
`devsecops` users have the ability to read (`Get*`, `List*`, …) anything about any AWS resource, via their group
membership. To be self-sufficient, `devsecops` users have the ability to change the policies of their group. This means
that, in essence, they can promote themselves to do anything, and manage their own membership. Members are however
responsible for their actions, and all actions are audited.

`devsecops` members are self-sufficient to do anything with AWS resources, for all projects, with team responsibility,
via _assume role_. I.e., users cannot make any changes directly, but can assume roles defined for specific applications,
and can define such roles.

`devsecops` members are expected to run tests, like the CI, from their local machines during development. Therefor,
`devsecops` members have the ability to assume the test roles defined by each application, like `ci` users. They also
have the ability to create such roles.

Apart from running tests, `devsecops` members are expected to create, maintain, and eventually destroy the
infrastructure needed for all applications of the organization, including the management of the execution and test
roles. This typically requires more capabilities that the test roles provide.

Each project defines an infrastructure policy (~ `terraform/meta/policy.tf`) that provides the minimal access rights to
manage the application's infrastructure. Optionally, the policy is parametrized for different environments. Access is
limited to avoid as many mistakes as possible, not because we want tight control. An infrastructure role
(`terraform/meta/main.tf`) has this policy attached.

It is not that simple, however. To actually perform infrastructure management, the user needs to assume this role. This
implies that the role must exist _before_ performing infrastructure management. This role is therefor defined in _a
separate [Terraform] configuration `terraform/meta`_, next to the actual infrastructure [Terraform] configuration
`terraform/infrastructure`, which is set up with an AWS provider that assumes the defined role. In the
[Terraform `assume_role` usage](https://www.terraform.io/docs/providers/aws/#assume_role), only the `role_arn` is to be
used. The `external_id` can be used to access roles in another AWS account. The `session_name` makes tracking possible
with concurrent executions in logs, but in our usage, Terraform blocks concurrent usage. An extra limiting `policy`
could be defined, but makes little sense in our scenarios.

We desire for the permissions defined in the roles to be minimal, and to only apply on the exact AWS resource instances
used for the application. This is simple for the execution and test roles: we first defined the resource instances, and
define the policies with references to the instances. For the infrastructure role however we often have a bootstrap
issue.

_If_ it is possible to define an identifying property before the resource instance is created, that can be used in a
policy that allows creation of the resource instance (via the ARN, or possibly via conditions), we do so in the
`terraform/meta` configuration, and use it in the infrastructure role's policy definition, and expose it via outputs to
the `terraform/infrastructure` configuration. There it can be used to create actual resource instance.

If not, we can only allow the infrastructure role to create _any_ resource of the needed type.

Note that the role does not "inherit" the capabilities of the `devsecops` users to read (`Get*`, `List*`, …) anything.
All necessary read-capabilities are added in the `terraform/meta` configuration, and are not required to be minimal.

The `terraform/infrastructure` configuration can in most cases expand the infrastructure role's capabilities to _change_
specifically created instances. However, in many instances, "changes" are part of the "creation" process, and it is not
good enough to add these capabilities after creation. In that case, also the ability to make changes must be added in
the `terraform/meta` configuration beforehand, often on the level of the resource type, rather than for a specific
instance.

Finally, the infrastructure role must have the capability to destroy the infrastructure. Sadly, this cannot be defined
for specific instances like the capability to change a specific resource instance in the `terraform/infrastructure`
configuration (if that is at all possible). This is because the definition of the extra capabilities will depend on the
definition of the actual resource instance. That is how [Terraform] knows that it must first realize the resource
instance, and add the extra capabilities later. When destroying the infrastructure however, [Terraform] will first
destroy the leaves of the dependency graph, in this case that capability to change _and destroy_ the specific instance,
before destroying the resource instance these capabilities depend on. This means that the capability to destroy the
resource instances will be gone before the attempt is made to actually destroy that resource instance. This means that
we must define the capability to destroy the resource instance together with the capability to create it, in the
`terraform/meta` configuration.

Sadly, often, it makes little sense to separate out the capability to change resources from the `terraform/meta`
configuration, and we end up with a `terraform/meta` configuration that allows _any_ action on _any_ instance of
relevant types. There does not seem to be a better solution.

All IAM users that manage the infrastructure must be able to assume this role. Therefor, the role has a trust policy
that includes

    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Principal": {
            "AWS": "*"
          },
          "Action": "sts:AssumeRole"
        }
      ],
      "Condition": {
        "StringLike": {
          "aws:PrincipalTag/canAssumeRole": "*infrastructure*"
        }
      }
    }

`devsecops` users can assume infrastructure and test roles when they are tagged (see below) with

    "canAssumeRole": "infrastructure+test"

The name of the role _MUST_ be of the form `role/infrastructure/*`.

The `devsecops` group policy allows members to assume the test and infrastructure roles:

    {
      "Version": "2012-10-17",
      "Statement": {
        "Effect": "Allow",
        "Action": "sts:AssumeRole",
        "Resource": [
          "arn:aws:iam::ACCOUNT-ID-WITHOUT-HYPHENS:role/infrastructure/*",
          "arn:aws:iam::ACCOUNT-ID-WITHOUT-HYPHENS:role/test/*"
        ]
      }
    }

Infrastructure roles do not allow usage of the AWS assets used for testing or execution, unless it is necessary for part
of the setup.

## Notes

### Making sure roles can be assumed

To create the execution, test, and infrastructure roles, see

[AWS Documentation » AWS Identity and Access Management » User Guide » Identities (Users, Groups, and Roles) » IAM Roles » Creating IAM Roles » Creating a Role to Delegate Permissions to an IAM User](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_create_for-user.html).

The usage is described in

[AWS Documentation » AWS Command Line Interface » User Guide » Configuring the AWS CLI » Assuming an IAM Role in the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-role.html)

and

[AWS Documentation » AWS Identity and Access Management » User Guide » Identities (Users, Groups, and Roles) » IAM Roles » Using IAM Roles » Granting a User Permissions to Switch Roles](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_use_permissions-to-switch.html).

The test (which includes the execution policy) and infrastructure roles must allow to be assumed by a user (principal),
with a policy that includes, respectively:

    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Principal": {
            "AWS": "*"
          },
          "Action": "sts:AssumeRole"
        }
      ],
      "Condition": {
        "StringLike": {
          "aws:PrincipalTag/canAssumeRole": "*test*"
        }
      }
    }

and

    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Principal": {
            "AWS": "*"
          },
          "Action": "sts:AssumeRole"
        }
      ],
      "Condition": {
        "StringLike": {
          "aws:PrincipalTag/canAssumeRole": "*infrastructure*"
        }
      }
    }

This is because it is not possible to limit which principals can assume roles to groups
([AWS Documentation » AWS Identity and Access Management » User Guide » Reference Information for AWS Identity and Access Management » IAM JSON Policy Reference » IAM JSON Policy Elements Reference » AWS JSON Policy Elements: Principal](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_elements_principal.html)).
Therefor, _tags_ are used to define `ci` and `devsecops` users (principals), on top of the group. The group is used to
defined general permissions. The tags are used to make it possible to assume the necessary roles, using a `condition`
([AWS Documentation » AWS Identity and Access Management » User Guide » Reference Information for AWS Identity and Access Management » IAM JSON Policy Reference » AWS Global Condition Context Keys](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_condition-keys.html#AvailableKeys)).
Note that we also cannot use the IAM user's ARN, based on the path, since we do not want `devsecops` members to be
limited to a hierarchical structure.

`ci` users are tagged with

    "canAssumeRole": "test"

`devsecops` users are tagged with

    "canAssumeRole": "infrastructure+test"

### Infrastructure maintenance by CI

Another approach would be to deny all write access to `devsecops` for all operational resources. Only CI should have the
rights to change operational resources. This is the only strategy that enforces that all changes to infrastructure are
committed, and go through QA.

Development and experimental resources can be changed by `devsecops`. Production and staging resources should never by
changed by humans.

However, the application of a [Terraform] configuration takes rather long, while most of the time is waiting. Since we
pay for CI execution by duration, this would get quite expensive. Furthermore, significant changes to infrastructure
tend not to succeed in one pass, which makes human oversight still necessary.

### Evolution

This is the 4th attempt to this goal.

In the first attempt, we created an empty "devsecops" group here, which only had the permission to add policies to
itself. Each project then defined a devsecops policy that had the minimal access rights to perform devsecops tasks for
that project, and added that policy to the "devsecops" group.

This attempt failed, because we discovered that we can add maximally 10 policies to a group.

In a variant, we contemplated for each project to extend a general policy for the devsecops group defined here. That
does not work, because the policy definitions have a limited size.

After study and discussion, an approach where an extra configuration would be created that depends on all projects and
this configuration, that adds the union of the necessary specialized rights for all projects in one go, was rejected as
intractable.

The 2nd attempt defined the union of the necessary rights for all projects in one go for the `devsecops` group. This
comes down to rights on types of resources, only sometimes limited to specific resources or groups of resources
according to name patters, and no longer limits access to minimal, specific resource instances and actions. It is
unstable, because specific rights are defined from many different applications, and this leads to drift quickly.
Furthermore, it ended up in everybody being able to do any thing on any instance with their account by default all the
time.

A third attempt started using "assume role" for the infrastructure configuration. It hit the limitations described
above.

The current approach at least has the benefit that the necessary rights are made explicit per application, and are not
directly associated with the `devsecops` users. Using conditions to limit access, with predefined identifiers, might be
the approach we are looking for.

## MUDO

- manage super users

[terraform]: https://peopleware.atlassian.net/wiki/x/CwAvBg
[getting started with a terraform configuration]: https://peopleware.atlassian.net/wiki/x/p4zhC
[aws credentials]: https://peopleware.atlassian.net/wiki/x/RoAWBg
