locals {
  path-ci = "/machine/ci/"

  canAssumeRole-ci = "test"
}

module "bitbucket_ppwcode" {
  source = "./user"
  name   = "bitbucket_ppwcode"
  path   = "${local.path-ci}"

  group_names = ["${aws_iam_group.ci.name}"]

  tags = {
    realName      = "Bitbucket ppwcode"
    canAssumeRole = "${local.canAssumeRole-ci}"
  }
}
