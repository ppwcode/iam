resource "aws_iam_group" "ci" {
  name = "ci"
  path = "/machine/"
}

resource "aws_iam_group_policy_attachment" "ci-test" {
  group      = "${aws_iam_group.ci.name}"
  policy_arn = "${aws_iam_policy.iam-assume_role-test.arn}"
}
