# Users can see (Get and List) (almost) anything. Aimed at devops users.
#
# Billing is not included.
#
# This does not avoid the need to add the read permissions to each project specific role policy, because the role that is assumed must have the read permissions, not the
# user.

# https://docs.aws.amazon.com/IAM/latest/UserGuide/list_iam.html

data "aws_iam_policy_document" "read_almost_anything" {
  version = "2012-10-17"

  statement {
    sid = "ReadAlmostAnything"

    effect = "Allow"

    # TODO add Get* and List* (and other read only permissions) for all services we use
    actions = [
      "cloudwatch:DescribeAlarms",
      "cloudwatch:GetMetricStatistics",
      "dynamodb:BatchGetItem",
      "dynamodb:ConditionCheckItem",
      "dynamodb:Get*",
      "dynamodb:List*",
      "dynamodb:Describe*",
      "dynamodb:Query",
      "dynamodb:Scan",
      "iam:Get*",
      "iam:List*",
      "kms:Get*",
      "kms:List*",
      "kms:DescribeKey",
      "route53:Get*",
      "route53:List*",
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_policy" "read_almost_anything" {
  name = "ReadAlmostAnything"
  path = "/general/"

  description = "can Get* and List* (read) read almost anything"

  policy = "${data.aws_iam_policy_document.read_almost_anything.json}"
}
