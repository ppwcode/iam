/* NOTE: users jdockx are considered root users, and are not under terraform control, to avoid
   lock out */

locals {
  path-human_peopleware = "/human/peopleware/"

  groupNames-devsecops = [
    "${aws_iam_group.humans.name}",
    "${aws_iam_group.devsecops.name}",
  ]

  canAssumeRole-devsecops = "infrastructure+test"
}

module "Jan_Dockx" {
  source = "./user"
  name   = "jand"
  path   = "${local.path-human_peopleware}"

  group_names = "${local.groupNames-devsecops}"

  tags = {
    realName      = "Jan Dockx"
    canAssumeRole = "${local.canAssumeRole-devsecops}"
  }
}

module "Adriaan_Peeters" {
  source = "./user"
  name   = "apeeters"
  path   = "${local.path-human_peopleware}"

  group_names = "${local.groupNames-devsecops}"

  tags = {
    realName      = "Adriaan Peeters"
    canAssumeRole = "${local.canAssumeRole-devsecops}"
  }
}

module "Ruben_Vandeginste" {
  source = "./user"
  name   = "rvandeginste"
  path   = "${local.path-human_peopleware}"

  group_names = "${local.groupNames-devsecops}"

  tags = {
    realName      = "Ruben Vandeginste"
    canAssumeRole = "${local.canAssumeRole-devsecops}"
  }
}
