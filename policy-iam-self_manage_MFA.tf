// https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_examples_iam_mfa-selfmanage.html

data "aws_iam_policy_document" "iam-self_manage_mfa" {
  version = "2012-10-17"

  statement {
    sid    = "AllowOwnMFAAdmin"
    effect = "Allow"

    actions = [
      "iam:CreateVirtualMFADevice",
      "iam:EnableMFADevice",
      "iam:ResyncMFADevice",
      "iam:DeleteVirtualMFADevice",
    ]

    resources = [
      "arn:aws:iam::*:mfa/$${aws:username}",
      "arn:aws:iam::*:user/human/*$${aws:username}",
    ]
  }

  statement {
    sid    = "AllowDeactivateOwnMFA"
    effect = "Allow"

    actions = [
      "iam:DeactivateMFADevice",
    ]

    resources = [
      "arn:aws:iam::*:mfa/$${aws:username}",
      "arn:aws:iam::*:user/human/*$${aws:username}",
    ]

    condition {
      test     = "Bool"
      variable = "aws:MultiFactorAuthPresent"
      values   = ["true"]
    }
  }

  statement {
    sid    = "AllowListMFAandUsersForConsole"
    effect = "Allow"

    actions = [
      "iam:ListMFADevices",
      "iam:ListVirtualMFADevices",
      "iam:ListUsers",
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_policy" "iam-self_manage_MFA" {
  name = "IamSelfManageMFA"
  path = "/iam/"

  description = "users can manage their MFA settings themselves"

  policy = "${data.aws_iam_policy_document.iam-self_manage_mfa.json}"
}
