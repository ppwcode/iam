# Devsecops can define execution, test and infrastructure policies and roles. Be careful.
#
# This policy does not describe Get*, List*, or Generate* actions, because we assume policy users already got those
# rights from `policy-iam-read.tf` and `policy-read_anything.tf`.

# https://docs.aws.amazon.com/IAM/latest/UserGuide/list_iam.html

# test and execution roles are defined by execution roles; devsecops does not have rights directly
data "aws_iam_policy_document" "iam-manage_roles-devsecops" {
  version = "2012-10-17"

  statement {
    sid    = "AllowIamDevsecopsPolicyManagement"
    effect = "Allow"

    actions = [
      "iam:CreatePolicy",
      "iam:DeletePolicy",
      "iam:CreatePolicyVersion",
      "iam:DeletePolicyVersion",
      "iam:SetDefaultPolicyVersion",
    ]

    resources = [
      "arn:aws:iam::${data.aws_caller_identity.caller.account_id}:policy/infrastructure/*",
    ]
  }

  statement {
    sid    = "AllowIamDevsecopsRoleManagement"
    effect = "Allow"

    actions = [
      "iam:CreateRole",
      "iam:UpdateRole",
      "iam:UpdateRoleDescription",
      "iam:TagRole",
      "iam:UntagRole",
      "iam:UpdateAssumeRolePolicy",
      "iam:DeleteRole",
      "iam:PutRolePolicy",
      "iam:AttachRolePolicy",
      "iam:DetachRolePolicy",
      "iam:DeleteRolePolicy",

      # allow users to assume these roles
      "sts:AssumeRole",
    ]

    resources = [
      "arn:aws:iam::${data.aws_caller_identity.caller.account_id}:role/infrastructure/*",
    ]
  }

  statement {
    sid    = "AllowIamAssumeRoleInfrastructure"
    effect = "Allow"

    actions = [
      # allow users to assume these roles
      "sts:AssumeRole",
    ]

    resources = [
      "arn:aws:iam::${data.aws_caller_identity.caller.account_id}:role/infrastructure/*",
    ]
  }
}

resource "aws_iam_policy" "iam-manage_roles-devsecops" {
  name = "IamManageRolesDevsecops"
  path = "/iam/"

  description = "can define roles and policies, and assume roles that have path /role/infrastructure/*"

  policy = "${data.aws_iam_policy_document.iam-manage_roles-devsecops.json}"
}
