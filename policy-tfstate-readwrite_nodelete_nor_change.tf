/* The Terraform state bucket is special. It is defined in another configuration, and can never be deleted.
   It is protected here specially.

   This cannot be defined in the project that defines the tfstate infrastructure, because that is a bootstrap
   configuration.

   See https://docs.aws.amazon.com/AmazonS3/latest/dev/using-with-s3-actions.html for all S3 actions.

   See https://docs.aws.amazon.com/IAM/latest/UserGuide/list_dynamodb.html and
   https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/api-permissions-reference.html for all DynamoDB
   actions.
*/

locals {
  arn-bucket-prefix       = "arn:aws:s3:::"
  arn-postfix             = ".ppwcode.org"
  arn-tfstate_bucket      = "${local.arn-bucket-prefix}tfstate${local.arn-postfix}"
  arn-tfstate_logs_bucket = "${local.arn-bucket-prefix}tfstate-log${local.arn-postfix}"
  arn-dynamodb-prefix     = "arn:aws:dynamodb:eu-west-1:${data.aws_caller_identity.caller.account_id}:table/"
  arn-tfstate_table       = "${local.arn-dynamodb-prefix}tfstate-lock${local.arn-postfix}"

  s3-object-readwrite_nodelete-actions = [
    "s3:AbortMultipartUpload",
    "s3:ListMultipartUploadParts",
    "s3:GetObject",
    "s3:GetObjectAcl",
    "s3:GetObjectTagging",
    "s3:GetObjectTorrent",
    "s3:GetObjectVersion",
    "s3:GetObjectVersionAcl",
    "s3:GetObjectVersionForReplication",
    "s3:GetObjectVersionTagging",
    "s3:GetObjectVersionTorrent",
    "s3:PutObject",
    "s3:PutObjectTagging",
    "s3:PutObjectVersionTagging",
    "s3:ReplicateObject",
    "s3:ReplicateTags",
  ]

  s3-object-delete_and_config_change-actions = [
    "s3:DeleteObject",
    "s3:DeleteObjectTagging",
    "s3:DeleteObjectVersion",
    "s3:DeleteObjectVersionTagging",
    "s3:PutObjectAcl",
    "s3:PutObjectVersionAcl",
    "s3:RestoreObject",
    "s3:ObjectOwnerOverrideToBucketOwner",
    "s3:ReplicateDelete",
  ]

  s3-bucket-readwrite_nodelete-actions = [
    "s3:ListBucket",
    "s3:ListBucketByTags",
    "s3:ListBucketVersions",
    "s3:ListBucketMultipartUploads",
    "s3:GetAccelerateConfiguration",
    "s3:GetAnalyticsConfiguration",
    "s3:GetBucketAcl",
    "s3:GetBucketCORS",
    "s3:GetBucketLocation",
    "s3:GetBucketLogging",
    "s3:GetBucketNotification",
    "s3:GetBucketPolicy",
    "s3:GetBucketRequestPayment",
    "s3:GetBucketTagging",
    "s3:GetBucketVersioning",
    "s3:GetBucketWebsite",
    "s3:GetInventoryConfiguration",
    "s3:GetIpConfiguration",
    "s3:GetLifecycleConfiguration",
    "s3:GetMetricsConfiguration",
    "s3:GetReplicationConfiguration",
  ]

  s3-bucket-create-actions = [
    "s3:CreateBucket",
  ]

  s3-bucket-delete-actions = [
    "s3:DeleteBucket",
    "s3:DeleteBucketPolicy",
    "s3:DeleteBucketWebsite",
  ]

  s3-bucket-config_change-actions = [
    "s3:PutAccelerateConfiguration",
    "s3:PutAnalyticsConfiguration",
    "s3:PutBucketAcl",
    "s3:PutBucketCORS",
    "s3:PutBucketLogging",
    "s3:PutBucketNotification",
    "s3:PutBucketPolicy",
    "s3:PutBucketRequestPayment",
    "s3:PutBucketTagging",
    "s3:PutBucketVersioning",
    "s3:PutBucketWebsite",
    "s3:PutInventoryConfiguration",
    "s3:PutIpConfiguration",
    "s3:PutLifecycleConfiguration",
    "s3:PutMetricsConfiguration",
    "s3:PutReplicationConfiguration",
  ]

  /* NOTE: documented, but not recognized by AWS:
    "s3:DeleteReplicationConfiguration",
    "s3:GetEncryptionConfiguration",
    "s3:PutEncryptionConfiguration",
   */


  /* NOTE: requires full * resource; is this obsolete, replaced by ListBucket?
    "s3:ListObjects",
   */

  dynamodb-readwrite_nodelete-actions = [
    "dynamodb:DescribeLimits",
    "dynamodb:DescribeReservedCapacity",
    "dynamodb:DescribeReservedCapacityOfferings",
    "dynamodb:ListTables",
    "dynamodb:ListBackups",
    "dynamodb:DescribeTimeToLive",
    "dynamodb:ListStreams",
    "dynamodb:ListTagsOfResource",
  ]
  dynamodb-table-readwrite_nodelete-actions = [
    "dynamodb:BatchGetItem",
    "dynamodb:BatchWriteItem",
    "dynamodb:DeleteItem",
    "dynamodb:DescribeTable",
    "dynamodb:GetItem",
    "dynamodb:PutItem",
    "dynamodb:Query",
    "dynamodb:Scan",
    "dynamodb:UpdateItem",
    "dynamodb:DescribeBackup",
    "dynamodb:DescribeContinuousBackups",
  ]
  dynamodb-index-readwrite_nodelete-actions = [
    "dynamodb:Query",
    "dynamodb:Scan",
  ]
  dynamodb-stream-readwrite_nodelete-actions = [
    "dynamodb:DescribeStream",
    "dynamodb:GetRecords",
    "dynamodb:GetShardIterator",
  ]

  /* NOTE: cannot deny, because it is global
    "dynamodb:PurchaseReservedCapacityOfferings",
    "dynamodb:TagResource",
    "dynamodb:UntagResource",
   */

  dynamodb-table-delete_and_config_change-actions = [
    "dynamodb:CreateTable",
    "dynamodb:DeleteTable",
    "dynamodb:UpdateTable",
    "dynamodb:CreateBackup",
    "dynamodb:DeleteBackup",
    "dynamodb:RestoreTableFromBackup",
  ]

  /* NOTE: documented, but not recognized by AWS:
    "dynamodb:UpdateTimeToLive"
   */
}

data "aws_iam_policy_document" "tfstate-readwrite_nodelete_nor_change" {
  version = "2012-10-17"

  statement {
    sid    = "AllowTfStateBucketsRead"
    effect = "Allow"

    actions = [
      "s3:ListAllMyBuckets",
      "s3:HeadBucket",
    ]

    /* NOTE: arn:aws:s3:::* is not accepted as resource for these actions */
    resources = ["*"]
  }

  statement {
    sid     = "AllowTfStateBucketReadWrite"
    effect  = "Allow"
    actions = "${local.s3-bucket-readwrite_nodelete-actions}"

    resources = [
      "${local.arn-tfstate_bucket}",
      "${local.arn-tfstate_logs_bucket}",
    ]
  }

  statement {
    sid     = "AllowTfStateObjectReadWrite"
    effect  = "Allow"
    actions = "${local.s3-object-readwrite_nodelete-actions}"

    resources = [
      "${local.arn-tfstate_bucket}/*",
      "${local.arn-tfstate_logs_bucket}/*",
    ]
  }

  statement {
    sid     = "ProhibitTfstateBucketCreate"
    effect  = "Deny"
    actions = "${local.s3-bucket-create-actions}"

    resources = [
      "${local.arn-tfstate_bucket}",
      "${local.arn-tfstate_logs_bucket}",
    ]
  }

  statement {
    sid     = "ProhibitTfstateBucketDelete"
    effect  = "Deny"
    actions = "${local.s3-bucket-delete-actions}"

    resources = [
      "${local.arn-tfstate_bucket}",
      "${local.arn-tfstate_logs_bucket}",
    ]
  }

  statement {
    sid     = "ProhibitTfstateBucketConfigChange"
    effect  = "Deny"
    actions = "${local.s3-bucket-config_change-actions}"

    resources = [
      "${local.arn-tfstate_bucket}",
      "${local.arn-tfstate_logs_bucket}",
    ]
  }

  statement {
    sid     = "ProhibitTfstateObjectDeleteAndConfigChange"
    effect  = "Deny"
    actions = "${local.s3-object-delete_and_config_change-actions}"

    resources = [
      "${local.arn-tfstate_bucket}/*",
      "${local.arn-tfstate_logs_bucket}/*",
    ]
  }

  statement {
    sid       = "AllowTfStateTablesRead"
    effect    = "Allow"
    actions   = "${local.dynamodb-readwrite_nodelete-actions}"
    resources = ["*"]
  }

  statement {
    sid       = "AllowTfStateTableReadWrite"
    effect    = "Allow"
    actions   = "${local.dynamodb-table-readwrite_nodelete-actions}"
    resources = ["${local.arn-tfstate_table}"]
  }

  statement {
    sid       = "AllowTfStateTableIndexReadWrite"
    effect    = "Allow"
    actions   = "${local.dynamodb-index-readwrite_nodelete-actions}"
    resources = ["${local.arn-tfstate_table}/index/*"]
  }

  statement {
    sid       = "AllowTfStateTableStreamReadWrite"
    effect    = "Allow"
    actions   = "${local.dynamodb-stream-readwrite_nodelete-actions}"
    resources = ["${local.arn-tfstate_table}/stream/*"]
  }

  statement {
    sid       = "ProhibitTfstateTableDeleteAndConfigChange"
    effect    = "Deny"
    actions   = "${local.dynamodb-table-delete_and_config_change-actions}"
    resources = ["${local.arn-tfstate_table}"]
  }
}

resource "aws_iam_policy" "tfstate-readwrite_nodelete_nor_change" {
  name = "TfStateReadWrite"
  path = "/tfstate/"

  description = "can read and write terraform state, but cannot delete state, nor delete the bucket; can read logs, but not delete them or the log bucket"

  policy = "${data.aws_iam_policy_document.tfstate-readwrite_nodelete_nor_change.json}"
}
