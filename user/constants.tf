locals {
  tags = {
    terraform-configuration = "iam"
    terraform-workspace     = "${terraform.workspace}"
  }
}
