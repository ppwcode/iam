resource "aws_iam_user" "user" {
  name = "${var.name}"
  path = "${var.path}"

  tags = "${merge(local.tags, var.tags)}"

  force_destroy = true
}

resource "aws_iam_user_group_membership" "group_membership" {
  user   = "${aws_iam_user.user.name}"
  groups = var.group_names
}
