variable "name" {
  type = "string"

  description = <<DESCRIPTION
Name of the IAM user resource. This is the user name, to be used to login. Mandatory.
For human users, the format is the first part of the user's email address in the organisation, e.g., `j.doe`, if the email address is
`j.doe@example.org`. For automated users,
DESCRIPTION
}

variable "path" {
  type = "string"

  description = <<DESCRIPTION
Do include a starting slash and a final slash, as per Terraform standard. The path of human users should be at least `'/human/<ORGANIZATION>/…'`.
For CI users, the path should be at least `'/ci/…'`.
DESCRIPTION
}

variable "group_names" {
  type = "list"

  description = <<DESCRIPTION
Optional list of the names of the groups this user should be a member of.
DESCRIPTION

  default = []
}

variable "tags" {
  type = "map"

  description = <<DESCRIPTION
Optional tags for the user (key /value).
DESCRIPTION

  default = {}
}
