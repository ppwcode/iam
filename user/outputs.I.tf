output "I-name" {
  value = "${aws_iam_user.user.name}"
}

output "I-path" {
  value = "${aws_iam_user.user.path}"
}

output "I-arn" {
  value = "${aws_iam_user.user.arn}"
}

output "I-unique_id" {
  value = "${aws_iam_user.user.unique_id}"
}
