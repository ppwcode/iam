# User

Defines an IAM user.

Users have the path `<var.path>` (where `var.path` starts and ends with a `'/'`.

Users can be destroyed.

This **does not add any access rights to the user**. The user **must** be added, e.g., to the group `/humans` to get the
associated permissions.

(Sadly, we cannot refer to groups all these users should belong to inside this module).
